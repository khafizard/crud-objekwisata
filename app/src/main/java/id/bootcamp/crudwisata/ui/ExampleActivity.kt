package id.bootcamp.crudwisata.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import id.bootcamp.crudwisata.ui.viewmodel.ExampleViewModel
import id.bootcamp.crudwisata.ui.viewmodel.MyViewModelFactory
import id.bootcamp.crudwisata.databinding.ActivityMainBinding

class ExampleActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: ExampleViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //Inisiasi Viewmodel
        viewModel = ViewModelProvider(
            this,
            MyViewModelFactory.getInstance(this)
        ).get(ExampleViewModel::class.java)

        viewModel.getUser()
    }
}