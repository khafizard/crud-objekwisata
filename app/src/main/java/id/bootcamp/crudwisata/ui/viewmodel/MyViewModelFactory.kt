package id.bootcamp.crudwisata.ui.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import id.bootcamp.crudwisata.data.MyRepository
import id.bootcamp.crudwisata.di.Injection

class MyViewModelFactory private constructor(private val myRepository: MyRepository) :
    ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ExampleViewModel::class.java)) {
            return ExampleViewModel(myRepository) as T
        } else if (modelClass.isAssignableFrom(BookingViewModel::class.java)) {
            return BookingViewModel(myRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class: " + modelClass.name)
    }

    companion object {
        @Volatile
        private var instance: MyViewModelFactory? = null
        fun getInstance(context: Context): MyViewModelFactory =
            instance ?: synchronized(this) {
                instance ?: MyViewModelFactory(Injection.provideExampleRepository(context))
            }.also { instance = it }
    }
}