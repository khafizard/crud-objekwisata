package id.bootcamp.crudwisata.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import id.bootcamp.crudwisata.databinding.ActivityPlaceListBinding
import id.bootcamp.crudwisata.ui.adapter.PlaceAdapter
import id.bootcamp.crudwisata.ui.viewmodel.BookingViewModel
import id.bootcamp.crudwisata.ui.viewmodel.MyViewModelFactory
import id.bootcamp.projectcrud.data.local.entity.TouristAttractionEntity

class PlaceListActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPlaceListBinding
    private lateinit var viewModel: BookingViewModel
    private lateinit var placeAdapter: PlaceAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPlaceListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(
            this,
            MyViewModelFactory.getInstance(this)
        ).get(BookingViewModel::class.java)

        viewModel.getAllPlaceFromRepo()
        viewModel.placeLiveData.observe(this) {
            if (it != null) {
                binding.rvPlace.layoutManager =
                    LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
                placeAdapter = PlaceAdapter(ArrayList(it))
                binding.rvPlace.adapter = placeAdapter
                placeAdapter.onPlaceItemListener = object : PlaceAdapter.OnPlaceItemListener {
                    override fun onItemClick(touristAttractionEntity: TouristAttractionEntity) {
                        val intent =
                            Intent(this@PlaceListActivity, PlaceDetailsActivity::class.java)
                        intent.putExtra("data", touristAttractionEntity.id)
                        startActivity(intent)
                    }
                }
            }
        }

        setUIDefault()

        binding.btnAdd.setOnClickListener {
            val intent =
                Intent(this@PlaceListActivity, AddUpdatePlaceActivity::class.java)
            startActivity(intent)
        }


    }

    private fun setUIDefault() {
        Glide.with(this)
            .load("https://img.freepik.com/free-vector/hand-drawn-clip-art-man-customer-service-call-center-job-office-worker-character_40876-3163.jpg?w=740&t=st=1703090850~exp=1703091450~hmac=0372d02295f4d6cccf386f6a0bd886d6acca6204f3b68a2628a51bf03060b6a4")
            .into(binding.civUserPhoto)


        Glide.with(this)
            .load("https://img.freepik.com/free-vector/flat-design-mountain-range-silhouette_23-2150491868.jpg?w=1060&t=st=1703091332~exp=1703091932~hmac=d83aa0ce2e6e483e304f8f81b9c4f209765e19a0923fd91b5a6ef0e32ba7d64a")
            .into(binding.ivMountain)

        Glide.with(this)
            .load("https://img.freepik.com/free-photo/tropical-beach_74190-188.jpg?w=1060&t=st=1703091414~exp=1703092014~hmac=268ca8dde2b97b8cc1376eb3a37a54a4bd0861e406da3c9206a967c577d00279")
            .into(binding.ivBeach)

        Glide.with(this)
            .load("https://img.freepik.com/free-photo/pathway-middle-green-leafed-trees-with-sun-shining-through-branches_181624-4539.jpg?size=626&ext=jpg&ga=GA1.2.1405050627.1699580658&semt=sph")
            .into(binding.ivForest)
    }

    override fun onResume() {
        super.onResume()
        viewModel.getAllPlaceFromRepo()
    }

}