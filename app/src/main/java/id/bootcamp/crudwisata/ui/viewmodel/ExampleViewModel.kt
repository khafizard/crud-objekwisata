package id.bootcamp.crudwisata.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bootcamp.crudwisata.data.MyRepository
import id.bootcamp.crudwisata.data.local.entity.LocalEntity
import kotlinx.coroutines.launch

class ExampleViewModel(private val myRepository: MyRepository) : ViewModel() {
    val userLiveData = MutableLiveData<LocalEntity>()

    fun getUser() = viewModelScope.launch {
        val user = myRepository.getLocalUser()
        userLiveData.postValue(user ?: LocalEntity())
    }
}