package id.bootcamp.crudwisata.ui

import android.content.Intent
import android.icu.text.NumberFormat
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import id.bootcamp.crudwisata.R
import id.bootcamp.crudwisata.databinding.ActivityPlaceDetailsBinding
import id.bootcamp.crudwisata.ui.viewmodel.BookingViewModel
import id.bootcamp.crudwisata.ui.viewmodel.MyViewModelFactory
import id.bootcamp.projectcrud.data.local.entity.TouristAttractionEntity
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import java.util.Locale

class PlaceDetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPlaceDetailsBinding
    private lateinit var viewModel: BookingViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPlaceDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // SET TOOLBAR
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        supportActionBar?.title = "Place Detail"
        // SET TOOLBAR

        val dataId: Int = intent.getIntExtra("data", 0)

        viewModel = ViewModelProvider(
            this,
            MyViewModelFactory.getInstance(this)
        ).get(BookingViewModel::class.java)

        viewModel.getPlaceByIdFromRepo(dataId)
        viewModel.placeByIdLiveData.observe(this) {
            setUI(it)
        }

    }

    private fun setUI(it: TouristAttractionEntity) {
        if (it.image == "") {
            Glide.with(this)
                .load("https://img.freepik.com/free-photo/wooden-bridge-koh-nangyuan-island-surat-thani-thailand_335224-1082.jpg?w=1060&t=st=1703096089~exp=1703096689~hmac=596daa2215c4fd7ff2900554a3d8b2534217062944a417891e947dcf42ebf40f")
                .into(binding.ivPhoto)
        } else {
            Glide.with(this).load(it.image).into(binding.ivPhoto)
        }
        val price = it.price.toInt()
        val formatPrice = NumberFormat.getNumberInstance(Locale("id", "ID")).format(price)
        binding.tvPrice.text = "Rp " + formatPrice
        binding.tvPlaceName.text = it.name
        binding.tvDescription.text = it.desc
        binding.tvLocation.text = it.address
    }

    // Inflate menu; this adds items to the action bar if it is present.
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_detail_place, menu)
        return true
    }

    // Handle action bar item clicks here.
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_update -> {
                val dataId: Int = intent.getIntExtra("data", 0)
                // Handle update action
                val intent =
                    Intent(this@PlaceDetailsActivity, AddUpdatePlaceActivity::class.java)
                intent.putExtra("data", dataId)
                startActivity(intent)
                return true
            }

            R.id.action_delete -> {
                // Handle delete action
                showDeleteConfirmationDialog()
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun showDeleteConfirmationDialog() {
        val dataId: Int = intent.getIntExtra("data", 0)
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setTitle("Confirmation")
        alertDialogBuilder.setMessage("Are you sure you want to delete?")
        alertDialogBuilder.setPositiveButton("Delete") { dialog, which ->
            // Handle delete action here
            viewModel.deletePlace(dataId)
            finish()
        }
        alertDialogBuilder.setNegativeButton("Cancel") { dialog, which ->
            // Dismiss the dialog
            dialog.dismiss()
        }
        val alertDialog = alertDialogBuilder.create()
        alertDialog.show()
    }

    override fun onResume() {
        super.onResume()
        val dataId: Int = intent.getIntExtra("data", 0)
        viewModel.getPlaceByIdFromRepo(dataId)
        viewModel.placeByIdLiveData.observe(this) {
            setUI(it)
        }
    }
}