package id.bootcamp.crudwisata.ui

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import id.bootcamp.crudwisata.R
import id.bootcamp.crudwisata.databinding.ActivityAddUpdatePlaceBinding
import id.bootcamp.crudwisata.ui.viewmodel.BookingViewModel
import id.bootcamp.crudwisata.ui.viewmodel.MyViewModelFactory
import id.bootcamp.projectcrud.data.local.entity.TouristAttractionEntity
import java.util.Date

class AddUpdatePlaceActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAddUpdatePlaceBinding
    private lateinit var viewModel: BookingViewModel
    private var mode = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddUpdatePlaceBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // SET TOOLBAR
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        supportActionBar?.title = "Add Place"
        // SET TOOLBAR

        viewModel = ViewModelProvider(
            this,
            MyViewModelFactory.getInstance(this)
        ).get(BookingViewModel::class.java)

        val dataId: Int = intent.getIntExtra("data", 0)

        var image = ""
        viewModel.placeByIdLiveData.observe(this) {
            binding.edtTitle.setText(it.name)
            binding.edtPrice.setText(it.price.toString())
            binding.edtDescription.setText(it.desc)
            binding.edtAddress.setText(it.address)
            image = it.image
        }

        if (dataId != 0) {
            viewModel.getPlaceByIdFromRepo(dataId)
            mode = "EDIT"
            binding.btnSubmit.setText("UPDATE")
        } else {
            mode = "TAMBAH"
            binding.btnSubmit.setText("ADD")
        }

        binding.btnSubmit.setOnClickListener {
            val name = binding.edtTitle.text.toString().trim()
            val price = binding.edtPrice.text.toString().trim()
            val desc = binding.edtDescription.text.toString().trim()
            val address = binding.edtAddress.text.toString().trim()

            var isValid = true

            if (name.isEmpty()) {
                binding.edtTitle.error = "Nama Tidak Boleh Kosong"
                isValid = false
            }
            if (price.isEmpty()) {
                binding.edtPrice.error = "Harga Tidak Boleh Kosong"
                isValid = false
            }
            if (desc.isEmpty()) {
                binding.edtDescription.error = "Deskripsi Tidak Boleh Kosong"
                isValid = false
            }
            if (address.isEmpty()) {
                binding.edtAddress.error = "Alamat Tidak Boleh Kosong"
                isValid = false
            }

            val hasilData = TouristAttractionEntity()
            hasilData.name = name
            hasilData.price = price.toInt()
            hasilData.desc = desc
            hasilData.address = address

            if (isValid) {
                if (mode == "TAMBAH") {
                    viewModel.insertPlace(hasilData)
                    finish()
                } else if (mode == "EDIT") {
                    hasilData.id = dataId
                    hasilData.image = image
                    viewModel.updatePlace(hasilData)
                    finish()
                }

            }

        }

    }


}