package id.bootcamp.crudwisata.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import id.bootcamp.crudwisata.R
import id.bootcamp.projectcrud.data.local.entity.TouristAttractionEntity

class PlaceAdapter(val placeList: ArrayList<TouristAttractionEntity>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val photo = itemView.findViewById<ImageView>(R.id.ivPhoto)
        val name = itemView.findViewById<TextView>(R.id.tvPlaceName)
        val location = itemView.findViewById<TextView>(R.id.tvLocation)
    }


    var onPlaceItemListener: OnPlaceItemListener? = null

    interface OnPlaceItemListener {
        fun onItemClick(touristAttractionEntity: TouristAttractionEntity)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_place, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return placeList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = placeList[position]
        if (holder is ViewHolder) {
            holder.name.text = data.name
            if (data.image == "") {
                Glide.with(holder.itemView.context)
                    .load("https://img.freepik.com/free-photo/wooden-bridge-koh-nangyuan-island-surat-thani-thailand_335224-1082.jpg?w=1060&t=st=1703096089~exp=1703096689~hmac=596daa2215c4fd7ff2900554a3d8b2534217062944a417891e947dcf42ebf40f")
                    .into(holder.photo)
            } else {
                Glide.with(holder.itemView.context)
                    .load(data.image)
                    .into(holder.photo)
            }
            val address = data.address.split(",").toTypedArray()
            holder.location.text = address[address.size - 1]
        }

        if (onPlaceItemListener != null) {
            holder.itemView.setOnClickListener {
                onPlaceItemListener?.onItemClick(data)
            }
        }
    }
}