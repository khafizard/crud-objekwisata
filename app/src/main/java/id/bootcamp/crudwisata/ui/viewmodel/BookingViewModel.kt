package id.bootcamp.crudwisata.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bootcamp.crudwisata.data.MyRepository
import id.bootcamp.crudwisata.data.local.entity.LocalEntity
import id.bootcamp.projectcrud.data.local.entity.TouristAttractionEntity
import kotlinx.coroutines.launch

class BookingViewModel(private val myRepository: MyRepository) : ViewModel() {
    val placeLiveData = MutableLiveData<List<TouristAttractionEntity>>()
    val placeByIdLiveData = MutableLiveData<TouristAttractionEntity>()

    fun getAllPlaceFromRepo() = viewModelScope.launch {
        val data = myRepository.getAllPlaceFromDao()
        placeLiveData.postValue(data)
    }

    fun getPlaceByIdFromRepo(id: Int) = viewModelScope.launch {
        val data = myRepository.getPlaceByIdFromDao(id)
        placeByIdLiveData.postValue(data)
    }

    fun insertPlace(touristAttractionEntity: TouristAttractionEntity) = viewModelScope.launch {
        myRepository.insertPlace(touristAttractionEntity)
    }

    fun updatePlace(touristAttractionEntity: TouristAttractionEntity) = viewModelScope.launch {
        myRepository.updatePlace(touristAttractionEntity)
    }

    fun deletePlace(id: Int) = viewModelScope.launch {
        myRepository.deletePlace(id)
    }
}