package id.bootcamp.crudwisata.di

import android.content.Context
import id.bootcamp.crudwisata.data.MyRepository
import id.bootcamp.crudwisata.data.local.room.MyDatabase

object Injection {
    fun provideExampleRepository(context: Context): MyRepository {
        val myDatabase = MyDatabase.getInstance(context)
        val localDao = myDatabase.localDao()
        val bookingDao = myDatabase.bookingDao()
        return MyRepository.getInstance(localDao, bookingDao)
    }


}