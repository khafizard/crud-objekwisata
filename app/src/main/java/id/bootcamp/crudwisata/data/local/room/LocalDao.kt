package id.bootcamp.crudwisata.data.local.room

import androidx.room.Dao
import androidx.room.Query
import id.bootcamp.crudwisata.data.local.entity.LocalEntity

@Dao
interface LocalDao {
    @Query("SELECT * FROM local")
    suspend fun getLocalUser(): LocalEntity?
}