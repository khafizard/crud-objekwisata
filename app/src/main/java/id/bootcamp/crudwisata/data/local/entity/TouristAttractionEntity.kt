package id.bootcamp.projectcrud.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tourist_attraction")
class TouristAttractionEntity(

    @field:ColumnInfo(name = "id")
    @field:PrimaryKey(autoGenerate = true)
    var id: Int = 0,

    @field:ColumnInfo(name = "name")
    var name: String = "",

    @field:ColumnInfo(name = "price")
    var price: Int = 0,

    @field:ColumnInfo(name = "image")
    var image: String = "",

    @field:ColumnInfo(name = "desc")
    var desc: String = "",

    @field:ColumnInfo(name = "address")
    var address: String = ""
)