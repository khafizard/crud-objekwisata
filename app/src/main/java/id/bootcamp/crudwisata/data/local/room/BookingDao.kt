package id.bootcamp.crudwisata.data.local.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import id.bootcamp.crudwisata.data.local.entity.LocalEntity
import id.bootcamp.projectcrud.data.local.entity.TouristAttractionEntity

@Dao
interface BookingDao {
    @Query("SELECT * FROM tourist_attraction")
    suspend fun getAllPlaceFromDb(): List<TouristAttractionEntity>

    @Query("SELECT * FROM tourist_attraction WHERE id = :id")
    suspend fun getPlaceByIdFromDb(id: Int): TouristAttractionEntity

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPlace(touristAttractionEntity: TouristAttractionEntity)

    @Query("DELETE FROM tourist_attraction WHERE ID = :id")
    suspend fun deletePlace(id: Int)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updatePlace(touristAttractionEntity: TouristAttractionEntity)


}