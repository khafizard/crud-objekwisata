package id.bootcamp.crudwisata.data

import id.bootcamp.crudwisata.data.local.entity.LocalEntity
import id.bootcamp.crudwisata.data.local.room.BookingDao
import id.bootcamp.crudwisata.data.local.room.LocalDao
import id.bootcamp.projectcrud.data.local.entity.TouristAttractionEntity

class MyRepository private constructor(
    private val localDao: LocalDao,
    private val bookingDao: BookingDao
) {
    suspend fun getLocalUser(): LocalEntity? {
        return localDao.getLocalUser()
    }

    suspend fun getAllPlaceFromDao(): List<TouristAttractionEntity> {
        return bookingDao.getAllPlaceFromDb()
    }

    suspend fun getPlaceByIdFromDao(id: Int): TouristAttractionEntity {
        return bookingDao.getPlaceByIdFromDb(id)
    }

    suspend fun insertPlace(touristAttractionEntity: TouristAttractionEntity) {
        bookingDao.insertPlace(touristAttractionEntity)
    }

    suspend fun updatePlace(touristAttractionEntity: TouristAttractionEntity) {
        bookingDao.updatePlace(touristAttractionEntity)
    }

    suspend fun deletePlace(id: Int) {
        bookingDao.deletePlace(id)
    }

    companion object {
        @Volatile
        private var instance: MyRepository? = null
        fun getInstance(
            localDao: LocalDao,
            bookingDao: BookingDao,
        ): MyRepository =
            instance ?: synchronized(this) {
                instance ?: MyRepository(localDao, bookingDao)
            }.also { instance = it }
    }
}