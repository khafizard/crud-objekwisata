package id.bootcamp.crudwisata.data.local.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import id.bootcamp.crudwisata.data.local.entity.LocalEntity
import id.bootcamp.projectcrud.data.local.entity.BookingEntity
import id.bootcamp.projectcrud.data.local.entity.TouristAttractionEntity
import id.bootcamp.projectcrud.data.local.entity.UserEntity

@TypeConverters(DateConverter::class)
@Database(
    entities = [LocalEntity::class, BookingEntity::class, TouristAttractionEntity::class, UserEntity::class],
    version = 2
)
abstract class MyDatabase : RoomDatabase() {

    abstract fun localDao(): LocalDao

    abstract fun bookingDao(): BookingDao

    companion object {
        @Volatile
        private var instance: MyDatabase? = null
        fun getInstance(context: Context): MyDatabase =
            instance ?: synchronized(this) {
                instance ?: Room.databaseBuilder(
                    context.applicationContext,
                    MyDatabase::class.java, "my-database.db"
                )
                    .createFromAsset("database/my-database.db")
                    //.addMigrations(migrations)
                    .build()
            }

        val migrations = object : Migration(2, 3) {
            override fun migrate(db: SupportSQLiteDatabase) {
            }
        }
    }
}