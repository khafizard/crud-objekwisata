package id.bootcamp.projectcrud.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user")
class UserEntity(

    @field:ColumnInfo(name = "id")
    @field:PrimaryKey
    var id: Int = 0,

    @field:ColumnInfo(name = "email")
    var email: String = "",

    @field:ColumnInfo(name = "password")
    var password: String = "",

    @field:ColumnInfo(name = "role")
    var role: String = "",

    @field:ColumnInfo(name = "name")
    var name: String = "",

    @field:ColumnInfo(name = "image")
    var image: String = ""
)