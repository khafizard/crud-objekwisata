package id.bootcamp.projectcrud.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.Date

@Entity(tableName = "booking")
class BookingEntity(

    @field:ColumnInfo(name = "id")
    @field:PrimaryKey
    var id: Int = 0,

    @field:ColumnInfo(name = "touristattraction_id")
    var touristAttractionId: Int = 0,

    @field:ColumnInfo(name = "user_id")
    var userId: Int = 0,

    @field:ColumnInfo(name = "booking_code")
    var bookingCode: Int = 0,

    @field:ColumnInfo(name = "booking_date")
    var bookingDate: Date = Date(),

    @field:ColumnInfo(name = "paid")
    var paid: Boolean = false,

    @field:ColumnInfo(name = "used_code")
    var usedCode: Boolean = false,

    @field:ColumnInfo(name = "expired_on")
    var expiredOn: Date = Date()


)